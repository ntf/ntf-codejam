'use strict';
var argv = process.argv;
var args = process.argv.slice(2);
//http://mathjs.org/examples/index.html
var math = require("mathjs");
/**
 * Node.JS script
 *
 * @author ntf123@gmail.com
 */
var CodeJamHelper = function (inFile, debug) {
    var fs = require('fs');
    var inLines = fs.readFileSync(inFile, 'utf-8').replace(/\r/g, '').split(
        '\n').filter(String);
    this.data = inLines;
    this.debug = debug;
    this.currentLine_ = 0;

    if (this.debug) {
        console.log(this.data);
    }
};

CodeJamHelper.prototype.nextLine = function () {
    this.currentLine_++;
    this.data[this.currentLine_];
    return this;
};

CodeJamHelper.prototype.getLine = function () {
    return this.data[this.currentLine_];
};
CodeJamHelper.prototype.getData = function () {
    return this.data[this.currentLine_].split(' ');
};

CodeJamHelper.prototype.solve = function (solver) {
    // The first line of the input gives the number of test cases, T. T test
    // cases follow.
    this.currentLine_ = 0;
    this.noOfTestCase = parseInt(this.getData()[0]);

    if (this.debug) {
        console.log("[debug] No of test case : " + this.noOfTestCase);
    }

    for (var counter_ = 1; counter_ <= this.noOfTestCase; counter_++) {
        if (this.debug) {
            console.log("=========CASE " + counter_ + " =========\n");
        }
        solver(counter_, this);
        if (this.debug) {
            console.log("\n");
        }
        if (args[2] && counter_ == args[2])
            break;

    }
};
CodeJamHelper.prototype.singleLineOutputHeaderOnly = function (caseNo) {
    console.log("Case #" + caseNo + ":");
};
CodeJamHelper.prototype.singleLineOutput = function (caseNo, answer) {
    console.log("Case #" + caseNo + ": " + answer);
};

/**
 * @param row
 * @param col
 */
CodeJamHelper.prototype.create2DArray = function (row, col) {
    var arr = new Array(row);
    for (var a = 0; a < row; a++) {
        arr[a] = new Array(col);
    }
    return arr;
}
Array.prototype.min = function () {
    return this.reduce(function (p, v) {
        return (p < v ? p : v);
    });
}

Array.prototype.max = function () {
    return this.reduce(function (p, v) {
        return (p > v ? p : v);
    });
}
// start
//arguments: 0 = in file ,1: debug flag
var helper = new CodeJamHelper(args[0], args[1] ? args[1] : undefined);

helper.solve(function (caseNo, h) {

    // var N = parseInt(helper.nextLine().getData()[0]);
    //  data = helper.nextLine().getData();


    //  helper.singleLineOutput(caseNo, deceutfulPoint + " " + normalPoint);

});