'use strict';
var argv = process.argv;
var args = process.argv.slice(2);

/**
 * Node.JS script
 * 
 * @author ntf123@gmail.com
 */
var CodeJamHelper = function(inFile, debug) {
    var fs = require('fs');
    var inLines = fs.readFileSync(inFile, 'utf-8').replace(/\r/g, '').split(
	    '\n').filter(String);
    this.data = inLines;
    this.debug = debug;
    this.currentLine_ = 0;

    if (this.debug) {
	console.log(this.data);
    }
};

CodeJamHelper.prototype.nextLine = function() {
    this.currentLine_++;
    this.data[this.currentLine_];
    return this;
};

CodeJamHelper.prototype.getLine = function() {
    return this.data[this.currentLine_];
};
CodeJamHelper.prototype.getData = function() {
    return this.data[this.currentLine_].split(' ');
};

CodeJamHelper.prototype.solve = function(solver) {
    // The first line of the input gives the number of test cases, T. T test
    // cases follow.
    this.currentLine_ = 0;
    this.noOfTestCase = parseInt(this.getData()[0]);

    if (this.debug) {
	console.log("[debug] No of test case : " + this.noOfTestCase);
    }

    for ( var counter_ = 1; counter_ <= this.noOfTestCase; counter_++) {
	if (this.debug) {
	    console.log("=========CASE " + counter_ + " =========\n");
	}
	solver(counter_, this);
	if (this.debug) {
	    console.log("\n");
	}
    }
};
CodeJamHelper.prototype.singleLineOutput = function(caseNo, answer) {
    console.log("Case #" + caseNo + ": " + answer);
};

/**
 * @param row
 * @param col
 */
CodeJamHelper.prototype.create2DArray = function(row, col) {
    var arr = new Array(row);
    for ( var a = 0; a < col; a++) {
	arr[a] = new Array(col);
    }
    return arr;
}

// start
var helper = new CodeJamHelper(args[0], args[1] ? args[1] : undefined);

// In this problem, you start with 0 cookies. You gain cookies at a rate of 2
// cookies per second, by clicking on a giant cookie. Any time you have at least
// C cookies, you can buy a cookie farm. Every time you buy a cookie farm, it
// costs you C cookies and gives you an extra F cookies per second.

// Once you have X cookies that you haven't spent on farms, you win! Figure out
// how long it will take you to win if you use the best possible strategy.

// The first line of the input gives the number of test cases, T. T lines
// follow. Each line contains three space-separated real-valued numbers: C, F
// and X, whose meanings are described earlier in the problem statement.

// C, F and X will each consist of at least 1 digit followed by 1 decimal point
// followed by from 1 to 5 digits. There will be no leading zeroes.
helper.solve(function(caseNo, h) {
    var firstLine = helper.nextLine().getData();
    var C = parseFloat(firstLine[0]);
    var F = parseFloat(firstLine[1]);
    var X = parseFloat(firstLine[2]);

    if (helper.debug) {
	console.log(C);
	console.log(F);
	console.log(X);
    }

    var farmCount = 0;
    var cookiesCount = 0.0;
    var baseRate = 2.0;
    var currentRate = function(c) {
	return baseRate + c * F;
    };

    var times = [];

    // Suppose C=500.0, F=4.0 and X=2000.0. Here's how the best possible
    // strategy plays out:

    // You start with 0 cookies, but producing 2 cookies per second.

    // After 250 seconds, you will have C=500 cookies and can buy a farm
    // that
    // produces F=4 cookies per second.
    // After buying the farm, you have 0 cookies, and your total cookie
    // production is 6 cookies per second.
    // The next farm will cost 500 cookies, which you can buy after
    // about
    // 83.3333333 seconds.
    // After buying your second farm, you have 0 cookies, and your total
    // cookie
    // production is 10 cookies per second.
    // Another farm will cost 500 cookies, which you can buy after 50
    // seconds.
    // After buying your third farm, you have 0 cookies, and your total
    // cookie
    // production is 14 cookies per second.
    // Another farm would cost 500 cookies, but it actually makes sense
    // not to
    // buy it: instead you can just wait until you have X=2000 cookies,
    // which
    // takes about 142.8571429 seconds.
    // var i = 100;
    while (true) {

	// if we need less time to reach X directly than buying a farm

	var buyOneMoreToWin = ((C - cookiesCount) / currentRate(farmCount))
		+ ((X - cookiesCount) / currentRate(farmCount + 1));

	var NotBuyToWin = ((X - cookiesCount) / currentRate(farmCount));

	if (helper.debug) {
	    console.log("baseRate (" + baseRate + ") + farmCount (" + farmCount
		    + ") * F = " + (baseRate + farmCount * F));
	    console.log("buyOneMoreToWin: " + buyOneMoreToWin
		    + " | NotBuyToWin: " + NotBuyToWin);
	}

	if (buyOneMoreToWin > NotBuyToWin) {
	    times.push(X / currentRate(farmCount));
	    break;
	}

	// if not, we better buy a farm
	if (helper.debug) {
	    console.log("we better buy a farm count :" + farmCount + " takes "
		    + ((C - cookiesCount) / currentRate(farmCount))
		    + " seconds");
	}
	times.push((C - cookiesCount) / currentRate(farmCount));
	cookiesCount = 0;
	farmCount++;
    }

    // For each test case, output one line containing "Case #x: y",
    // where x
    // is
    // the test case number (starting from 1) and y is the minimum
    // number of
    // seconds it takes before you can have X delicious cookies.
    if (helper.debug) {
	console.log(times.length);
	// console.log(times);
    }
    helper.singleLineOutput(caseNo, parseFloat(times.reduce(function(pv, cv) {
	return pv + cv;
    }, 0)).toFixed(7));
});

function array_intersect(arr1) {
    // discuss at: http://phpjs.org/functions/array_intersect/
    // original by: Brett Zamir (http://brett-zamir.me)
    // note: These only output associative arrays (would need to be
    // note: all numeric and counting from zero to be numeric)
    // example 1: $array1 = {'a' : 'green', 0:'red', 1: 'blue'};
    // example 1: $array2 = {'b' : 'green', 0:'yellow', 1:'red'};
    // example 1: $array3 = ['green', 'red'];
    // example 1: $result = array_intersect($array1, $array2, $array3);
    // returns 1: {0: 'red', a: 'green'}

    var retArr = {}, argl = arguments.length, arglm1 = argl - 1, k1 = '', arr = {}, i = 0, k = '';

    arr1keys: for (k1 in arr1) {
	arrs: for (i = 1; i < argl; i++) {
	    arr = arguments[i];
	    for (k in arr) {
		if (arr[k] === arr1[k1]) {
		    if (i === arglm1) {
			retArr[k1] = arr1[k1];
		    }
		    // If the innermost loop always leads at least once to an
		    // equal value, continue the loop until done
		    continue arrs;
		}
	    }
	    // If it reaches here, it wasn't found in at least one array, so try
	    // next value
	    continue arr1keys;
	}
    }

    return retArr;
}
function array_values(input) {
    // discuss at: http://phpjs.org/functions/array_values/
    // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Brett Zamir (http://brett-zamir.me)
    // example 1: array_values( {firstname: 'Kevin', surname: 'van Zonneveld'}
    // );
    // returns 1: {0: 'Kevin', 1: 'van Zonneveld'}

    var tmp_arr = [], key = '';

    if (input && typeof input === 'object' && input.change_key_case) { // Duck-type
	// check
	// for
	// our
	// own
	// array()-created
	// PHPJS_Array
	return input.values();
    }

    for (key in input) {
	tmp_arr[tmp_arr.length] = input[key];
    }

    return tmp_arr;
}