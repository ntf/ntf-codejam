'use strict';
var argv = process.argv;
var args = process.argv.slice(2);

/**
 * Node.JS script
 * 
 * @author ntf123@gmail.com
 */
var CodeJamHelper = function(inFile, debug) {
    var fs = require('fs');
    var inLines = fs.readFileSync(inFile, 'utf-8').replace(/\r/g, '').split(
	    '\n').filter(String);
    this.data = inLines;
    this.debug = debug;
    this.currentLine_ = 0;

    if (this.debug) {
	console.log(this.data);
    }
};

CodeJamHelper.prototype.nextLine = function() {
    this.currentLine_++;
    this.data[this.currentLine_];
    return this;
};

CodeJamHelper.prototype.getLine = function() {
    return this.data[this.currentLine_];
};
CodeJamHelper.prototype.getData = function() {
    return this.data[this.currentLine_].split(' ');
};

CodeJamHelper.prototype.solve = function(solver) {
    // The first line of the input gives the number of test cases, T. T test
    // cases follow.
    this.currentLine_ = 0;
    this.noOfTestCase = parseInt(this.getData()[0]);

    if (this.debug) {
	console.log("[debug] No of test case : " + this.noOfTestCase);
    }

    for ( var counter_ = 1; counter_ <= this.noOfTestCase; counter_++) {
	if (this.debug) {
	    console.log("=========CASE " + counter_ + " =========\n");
	}
	solver(counter_, this);
	if (this.debug) {
	    console.log("\n");
	}
	if (args[2] && counter_ == args[2])
	    break;
    }
};
CodeJamHelper.prototype.singleLineOutputHeaderOnly = function(caseNo) {
    console.log("Case #" + caseNo + ":");
};
CodeJamHelper.prototype.singleLineOutput = function(caseNo, answer) {
    console.log("Case #" + caseNo + ": " + answer);
};

/**
 * @param row
 * @param col
 */
CodeJamHelper.prototype.create2DArray = function(row, col) {
    var arr = new Array(row);
    for ( var a = 0; a < row; a++) {
	arr[a] = new Array(col);
    }
    return arr;
}

// start
var helper = new CodeJamHelper(args[0], args[1] ? args[1] : undefined);

// Minesweeper is a computer game that became popular in the 1980s, and is still
// included in some versions of the Microsoft Windows operating system. This
// problem has a similar idea, but it does not assume you have played
// Minesweeper.

// In this problem, you are playing a game on a grid of identical cells. The
// content of each cell is initially hidden. There are M mines hidden in M
// different cells of the grid. No other cells contain mines. You may click on
// any cell to reveal it. If the revealed cell contains a mine, then the game is
// over, and you lose. Otherwise, the revealed cell will contain a digit between
// 0 and 8, inclusive, which corresponds to the number of neighboring cells that
// contain mines. Two cells are neighbors if they share a corner or an edge.
// Additionally, if the revealed cell contains a 0, then all of the neighbors of
// the revealed cell are automatically revealed as well, recursively. When all
// the cells that don't contain mines have been revealed, the game ends, and you
// win.

helper.solve(function(caseNo, h) {
    var firstLine = helper.nextLine().getData();

    var EMPTY = '.';
    var UNKNOWN = '#';
    var MINE = '*';
    var CLICKED = 'c';

    // Each line contains three space-separated integers: R, C, and M.
    // board is R (row) x C (col) , M = no of mimes hidden
    var R = parseFloat(firstLine[0]);
    var C = parseFloat(firstLine[1]);
    var M = parseFloat(firstLine[2]);

    var grid = helper.create2DArray(R, C);
    var printBoard = function(board) {
	for ( var i = 0; i < R; i++) {
	    console.log(board[i].toString().replace(/,/g, ''));
	}
    };

    if (helper.debug) {
	console.log([ R, C, M ]);
    }

    var counter = 0;
    var noOfBlankRemain = R * C - M;

    var midR = Math.ceil(R / 2);
    var midC = Math.ceil(C / 2);

    var check = function(a, b) {
	// if inside the board
	// console.log([a,b]);
	if (a >= 0 && b >= 0 && a < R && b < C) {
	    // check if not mine
	    return grid[a][b] != MINE;
	} else {

	    return true;
	}
    };
    var checkAndSet = function(a, b, c) {
	if (a >= 0 && b >= 0 && a < R && b < C) {
	    grid[a][b] = c;
	    return 1;
	} else {
	    return 0;
	}

    };
    grid[midR][midC] = CLICKED;
    noOfBlankRemain--;

    var i = midR, j = midC;
    while (true) {
	var count = 0;
	var setted = 0;
	if (check(i - 1, j)) {
	    setted += checkAndSet(i - 1, j, UNKNOWN);
	    count++;
	}
	if (check(i + 1, j)) {
	    setted += checkAndSet(i + 1, j, UNKNOWN);
	    count++;
	}
	if (check(i, j - 1)) {
	    setted += checkAndSet(i, j - 1, UNKNOWN);
	    count++;
	}

	if (check(i, j + 1)) {
	    setted += checkAndSet(i, j + 1, UNKNOWN);
	    count++;
	}

	if (check(i - 1, j - 1)) {
	    setted += checkAndSet(i - 1, j - 1, UNKNOWN);
	    count++;
	}
	if (check(i - 1, j + 1)) {
	    setted += checkAndSet(i - 1, j + 1, UNKNOWN);
	    count++;
	}
	if (check(i + 1, j - 1)) {
	    setted += checkAndSet(i + 1, j - 1, UNKNOWN);
	    count++;
	}
	if (check(i + 1, j + 1)) {
	    setted += checkAndSet(i + 1, j + 1, UNKNOWN);
	    count++;
	}
	
	if(count == 8){
	    //it's good to click this
	}
    }
    /*
     * for ( var i = 0; i < R; i++) { for ( var j = 0; j < C; j++) { counter++;
     * if (counter <= M) { grid[i][j] = MINE; } else { grid[i][j] = UNKNOWN; } } }
     */

    if (helper.debug) {
	printBoard(grid);
	console.log([ midR, midC ]);
    }

    if (false) {

	helper.singleLineOutputHeaderOnly(caseNo);
	printBoard(grid);
    } else {
	helper.singleLineOutputHeaderOnly(caseNo);
	printBoard(grid);
	console.log('Impossible');
	// breakpoint;
    }
    // Output

    // For each test case, output a line containing "Case #x:", where x
    // is the
    // test case number (starting from 1). On the following R lines,
    // output the
    // board configuration with C characters per line, using '.' to
    // represent an
    // empty cell, '*' to represent a cell that contains a mine, and 'c'
    // to
    // represent the clicked cell.

    // If there is no possible configuration, then instead of the grid,
    // output a
    // line with "Impossible" instead. If there are multiple possible
    // configurations, output any one of them.

    // helper.singleLineOutput(caseNo);
});
