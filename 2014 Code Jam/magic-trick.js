'use strict';
var argv = process.argv;
var args = process.argv.slice(2);

/**
 * Node.JS script
 * @author ntf123@gmail.com
 */
var CodeJamHelper = function(inFile, debug) {
    var fs = require('fs');
    var inLines = fs.readFileSync(inFile, 'utf-8').replace(/\r/g, '').split(
	    '\n').filter(String);
    this.data = inLines;
    this.debug = debug;
    this.currentLine_ = 0;

    if (this.debug) {
	console.log(this.data);
    }
};

CodeJamHelper.prototype.nextLine = function() {
    this.currentLine_++;
    this.data[this.currentLine_];
    return this;
};

CodeJamHelper.prototype.getLine = function() {
    return this.data[this.currentLine_];
};
CodeJamHelper.prototype.getData = function() {
    return this.data[this.currentLine_].split(' ');
};

CodeJamHelper.prototype.solve = function(solver) {
    // The first line of the input gives the number of test cases, T. T test
    // cases follow.
    this.currentLine_ = 0;
    this.noOfTestCase = parseInt(this.getData()[0]);

    if (this.debug) {
	console.log("[debug] No of test case : " + this.noOfTestCase);
    }

    for ( var counter_ = 1; counter_ <= this.noOfTestCase; counter_++) {
	if (this.debug) {
	    console.log("=========CASE " + counter_ + " =========\n");
	}
	solver(counter_, this);
	if (this.debug) {
	    console.log("\n");
	}
    }
};
CodeJamHelper.prototype.singleLineOutput = function(caseNo, answer) {
    console.log("Case #" + caseNo + ": " + answer);
};

/**
 * @param row
 * @param col
 */
CodeJamHelper.prototype.create2DArray = function(row, col) {
    var arr = new Array(row);
    for ( var a = 0; a < col; a++) {
	arr[a] = new Array(col);
    }
    return arr;
}

// start
var helper = new CodeJamHelper(args[0], args[1] ? args[1] : undefined);

helper.solve(function(caseNo, h) {
    // Each test case starts with a line containing an integer: the answer to
    // the first question. The next 4 lines represent the first arrangement of
    // the cards: each contains 4 integers, separated by a single space. The
    // next line contains the answer to the second question, and the following
    // four lines contain the second arrangement in the same format.
    var ans1 = parseInt(helper.nextLine().getData()[0]);
    var grid1 = helper.create2DArray(4, 4);
    var grid2 = helper.create2DArray(4, 4);
    for ( var i = 0; i < 4; i++) {
	// helper.nextLine();
	grid1[i] = helper.nextLine().getData();
    }

    var ans2 = parseInt(helper.nextLine().getData()[0]);
    for ( var i = 0; i < 4; i++) {
	// helper.nextLine();
	grid2[i] = helper.nextLine().getData();
    }

    // Output

    // For each test case, output one line containing "Case #x: y", where x is
    // the test case number (starting from 1).

    // If there is a single card the volunteer could have chosen, y should be
    // the number on the card. If there are multiple cards the volunteer could
    // have chosen, y should be "Bad magician!", without the quotes. If there
    // are no cards consistent with the volunteer's answers, y should be
    // "Volunteer cheated!", without the quotes. The text needs to be exactly
    // right, so consider copying/pasting it from here.
    var intersect = array_values(array_intersect(grid1[ans1 - 1],
	    grid2[ans2 - 1]));

    if (helper.debug) {
	console.log(grid1);
	console.log(grid2);
	console.log(intersect);
    }

    if (intersect.length == 1) {
	helper.singleLineOutput(caseNo, intersect[0]);
    } else if (intersect.length > 1) {
	helper.singleLineOutput(caseNo, "Bad magician!");
    } else if (intersect.length == 0) {
	helper.singleLineOutput(caseNo, "Volunteer cheated!");
    }
});

function array_intersect(arr1) {
    // discuss at: http://phpjs.org/functions/array_intersect/
    // original by: Brett Zamir (http://brett-zamir.me)
    // note: These only output associative arrays (would need to be
    // note: all numeric and counting from zero to be numeric)
    // example 1: $array1 = {'a' : 'green', 0:'red', 1: 'blue'};
    // example 1: $array2 = {'b' : 'green', 0:'yellow', 1:'red'};
    // example 1: $array3 = ['green', 'red'];
    // example 1: $result = array_intersect($array1, $array2, $array3);
    // returns 1: {0: 'red', a: 'green'}

    var retArr = {}, argl = arguments.length, arglm1 = argl - 1, k1 = '', arr = {}, i = 0, k = '';

    arr1keys: for (k1 in arr1) {
	arrs: for (i = 1; i < argl; i++) {
	    arr = arguments[i];
	    for (k in arr) {
		if (arr[k] === arr1[k1]) {
		    if (i === arglm1) {
			retArr[k1] = arr1[k1];
		    }
		    // If the innermost loop always leads at least once to an
		    // equal value, continue the loop until done
		    continue arrs;
		}
	    }
	    // If it reaches here, it wasn't found in at least one array, so try
	    // next value
	    continue arr1keys;
	}
    }

    return retArr;
}
function array_values(input) {
    // discuss at: http://phpjs.org/functions/array_values/
    // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Brett Zamir (http://brett-zamir.me)
    // example 1: array_values( {firstname: 'Kevin', surname: 'van Zonneveld'}
    // );
    // returns 1: {0: 'Kevin', 1: 'van Zonneveld'}

    var tmp_arr = [], key = '';

    if (input && typeof input === 'object' && input.change_key_case) { // Duck-type
	// check
	// for
	// our
	// own
	// array()-created
	// PHPJS_Array
	return input.values();
    }

    for (key in input) {
	tmp_arr[tmp_arr.length] = input[key];
    }

    return tmp_arr;
}