'use strict';
var argv = process.argv;
var args = process.argv.slice(2);

/**
 * Node.JS script
 * 
 * @author ntf123@gmail.com
 */
var CodeJamHelper = function(inFile, debug) {
    var fs = require('fs');
    var inLines = fs.readFileSync(inFile, 'utf-8').replace(/\r/g, '').split(
	    '\n').filter(String);
    this.data = inLines;
    this.debug = debug;
    this.currentLine_ = 0;

    if (this.debug) {
	console.log(this.data);
    }
};

CodeJamHelper.prototype.nextLine = function() {
    this.currentLine_++;
    this.data[this.currentLine_];
    return this;
};

CodeJamHelper.prototype.getLine = function() {
    return this.data[this.currentLine_];
};
CodeJamHelper.prototype.getData = function() {
    return this.data[this.currentLine_].split(' ');
};

CodeJamHelper.prototype.solve = function(solver) {
    // The first line of the input gives the number of test cases, T. T test
    // cases follow.
    this.currentLine_ = 0;
    this.noOfTestCase = parseInt(this.getData()[0]);

    if (this.debug) {
	console.log("[debug] No of test case : " + this.noOfTestCase);
    }

    for ( var counter_ = 1; counter_ <= this.noOfTestCase; counter_++) {
	if (this.debug) {
	    console.log("=========CASE " + counter_ + " =========\n");
	}
	solver(counter_, this);
	if (this.debug) {
	    console.log("\n");
	}
	if (args[2] && counter_ == args[2])
	    break;

    }
};
CodeJamHelper.prototype.singleLineOutputHeaderOnly = function(caseNo) {
    console.log("Case #" + caseNo + ":");
};
CodeJamHelper.prototype.singleLineOutput = function(caseNo, answer) {
    console.log("Case #" + caseNo + ": " + answer);
};

/**
 * @param row
 * @param col
 */
CodeJamHelper.prototype.create2DArray = function(row, col) {
    var arr = new Array(row);
    for ( var a = 0; a < row; a++) {
	arr[a] = new Array(col);
    }
    return arr;
}
Array.prototype.min = function() {
    return this.reduce(function(p, v) {
	return (p < v ? p : v);
    });
}

Array.prototype.max = function() {
    return this.reduce(function(p, v) {
	return (p > v ? p : v);
    });
}
// start
var helper = new CodeJamHelper(args[0], args[1] ? args[1] : undefined);

helper
	.solve(function(caseNo, h) {

	    // Each test case starts with a line containing a single integer N,
	    // the
	    // number of blocks each player has. Next follows a line containing
	    // N
	    // space-separated real numbers: the masses of Naomi's blocks, in
	    // kg.
	    // Finally there will be a line containing N space-separated real
	    // numbers:
	    // the masses of Ken's blocks, in kg.
	    var N = parseInt(helper.nextLine().getData()[0]);

	    var naomiRAW = [];
	    var kenRAW = [];
	    var naomi = [];
	    var ken = [];
	    var data;

	    data = helper.nextLine().getData();
	    for ( var i = 0; i < N; i++) {
		naomiRAW[i] = parseFloat(data[i]);
	    }
	    data = helper.nextLine().getData();
	    for ( var i = 0; i < N; i++) {
		kenRAW[i] = parseFloat(data[i]);
	    }

	    naomiRAW.sort();
	    kenRAW.sort();
	    if (helper.debug) {
		console.log([ naomiRAW, kenRAW ]);
	    }
	    var assert = function(v1, v2) {
		if (v1 != v2) {
		    throw Error("assertion error");
		}
	    };
	    // For each test case, output one line containing "Case #x: y z",
	    // where x is
	    // the test case number (starting from 1), y is the number of points
	    // Naomi
	    // will score if she plays Deceitful War optimally, and z is the
	    // number of
	    // points Naomi will score if she plays War optimally.

	    /**
	     * true = Naomi get one point
	     */
	    var kenStrategy = function(ken, against) {
		var found = false;
		for ( var i = 0; i < ken.length; i++) {
		    if (ken[i] > against) {
			// ken use the one just greater than mine
			ken.splice(i, 1);
			found = true;
			break;
		    }
		}
		// if not, then ken use his lowest
		if (found === false) {
		    ken.shift();
		    return true;
		} else {
		    return false;
		}
	    }
	    // Deceitful War
	    var deceutfulPoint = 0;

	    var naomi = naomiRAW.slice(0);
	    var ken = kenRAW.slice(0);
	    while (naomi.length > 0) {

		var kenLowest = ken[0];
		var sheUse = 0;
		var found = false;
		for ( var i = 0; i < naomi.length; i++) {
		    if (naomi[i] > kenLowest) {
			sheUse = naomi[i];
			// ken use the one just greater than mine
			naomi.splice(i, 1);
			found = true;
			break;
		    }
		}

		if (found) {
		    found = false;
		    var result = kenStrategy(ken, 1.0);
		    // then ken will use his lowest block , which she win
		    if (helper.debug) {
			console.log("use" +sheUse+ " to beat" +kenLowest);
		    }
		    if (result === true) {
			deceutfulPoint++;
		    }
		} else if (naomi[naomi.length - 1] < ken[ken.length - 1]) {
		    if (helper.debug) {
			console
				.log([
					"she use her smallest block to fake him use his greatest block",
					naomi[naomi.length - 1],
					ken[ken.length - 1] ]);
		    }

		    naomi.shift();
		    var result = kenStrategy(ken, ken[ken.length - 1] - 0.00001);
		    // assert(result, false);
		    if (result === true) {
			deceutfulPoint++;
		    }
		} else if (naomi[0] > ken[0]) {
		    if (helper.debug) {
			console.log("force him to use smallest and she win ");
		    }

		    naomi.shift();
		    var result = kenStrategy(ken, 1.0);
		    // then ken will use his lowest block , which she win
		    // assert(result, true);
		    if (result === true) {
			deceutfulPoint++;
		    }

		} else if (naomi[naomi.length - 1] > ken[ken.length - 1]) {
		    if (helper.debug) {
			console.log([ "she has something bigger than him.",
				naomi[naomi.length - 1], ken[ken.length - 1] ]);
		    }

		    var result = kenStrategy(ken, naomi.pop());
		    assert(result, true);
		    if (result === true) {
			deceutfulPoint++;
		    }
		} else if (naomi[0] < ken[0]) {
		    if (helper.debug) {
			console.log("naomi[0] < ken[0]");
		    }// naomi.shift();
		    var result = kenStrategy(ken, naomi.shift());
		    if (result === true) {
			deceutfulPoint++;
		    }
		}

		if (helper.debug) {
		    console.log([ naomi, ken ]);
		}
	    }

	    // sometimes there is a better strategy
	    var deceutfulPoint2 = 0;

	    var naomi = naomiRAW.slice(0);
	    var ken = kenRAW.slice(0);
	    while (naomi.length > 0) {

		if (naomi[0] > ken[0]) {
		    if (helper.debug) {
			console
				.log("she fake him by a value greater than largest of ken ");
		    }

		    naomi.shift();
		    var result = kenStrategy(ken, 1.0);
		    // then ken will use his lowest block , which she win
		    // assert(result, true);
		    if (result === true) {
			deceutfulPoint2++;
		    }
		} else if (naomi[naomi.length - 1] > ken[ken.length - 1]) {
		    if (helper.debug) {
			console.log([ "she has something bigger than him.",
				naomi[naomi.length - 1], ken[ken.length - 1] ]);
		    }

		    var result = kenStrategy(ken, naomi.pop());
		    assert(result, true);
		    if (result === true) {
			deceutfulPoint2++;
		    }
		} else if (naomi[0] < ken[0]) {
		    if (helper.debug) {
			console.log("naomi[0] < ken[0]");
		    }// naomi.shift();
		    var result = kenStrategy(ken, naomi.shift());
		    if (result === true) {
			deceutfulPoint2++;
		    }

		} else if (naomi[naomi.length - 1] < ken[ken.length - 1]) {
		    if (helper.debug) {
			console
				.log([
					"she use her smallest block to fake him use his greatest block",
					naomi[naomi.length - 1],
					ken[ken.length - 1] ]);
		    }

		    naomi.shift();
		    var result = kenStrategy(ken, ken[ken.length - 1] - 0.00001);
		    // assert(result, false);
		    if (result === true) {
			deceutfulPoint2++;
		    }
		}

		if (helper.debug) {
		    console.log([ naomi, ken ]);
		}
	    }

	    deceutfulPoint = deceutfulPoint2 > deceutfulPoint ? deceutfulPoint2
		    : deceutfulPoint;
	    // play normally
	    var normalPoint = 0;

	    var naomi = naomiRAW.slice(0);
	    var ken = kenRAW.slice(0);
	    while (naomi.length > 0) {
		var naomiLowest = naomi.shift();
		var result = kenStrategy(ken, naomiLowest);
		if (result === true) {
		    normalPoint++;
		}

		if (helper.debug) {
		    console.log([ naomi, ken ]);
		}
	    }

	    helper.singleLineOutput(caseNo, deceutfulPoint + " " + normalPoint);

	    // if (helper.debug) {
	    var noKenLargerThanShe = 0;
	    for ( var i = kenRAW.length - 1; i >= 0; i--) {
		if (kenRAW[i] > naomiRAW[naomiRAW.length - 1]) {
		    noKenLargerThanShe++;
		} else {
		    break;
		}
	    }

	    var noKenLargerThanShe2 = 0;
	    for ( var i = kenRAW.length - 1; i >= 0; i--) {
		if (kenRAW[i] > naomiRAW[naomiRAW.length - 1]) {
		    noKenLargerThanShe++;
		} else {
		    break;
		}
	    }
	    var nope = naomiRAW[naomiRAW.length - 1] < kenRAW[0]
		    || naomiRAW.length == deceutfulPoint
		    || ((naomiRAW.length - noKenLargerThanShe) == deceutfulPoint);
	/*    console
		    .log([
			    naomiRAW.length,
			    noKenLargerThanShe,
			    naomiRAW[naomiRAW.length - 1] < kenRAW[0],
			    naomiRAW.length == deceutfulPoint,
			    ((naomiRAW.length - noKenLargerThanShe) == deceutfulPoint) ]);*/
	   /* if (nope)
		console.log("[" + nope + "]");*/
	    // }
	    // helper.singleLineOutput(caseNo);
	});
