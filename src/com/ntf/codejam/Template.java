package com.ntf.codejam;

import java.io.*;
import java.util.Arrays;
import java.util.Locale;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class Template implements Runnable {

	private PrintWriter out;
	final String file = "code-jam-2014-round-1a/B/B-sample";
	Random rnd = new Random(42);

	/**
	 * Solution here
	 */
	static class Solution implements ISolution{
		
		Solution() {
			
		}
		
		@Override
		public void input(CodeJamReader in) throws IOException {
            
			
		}
		
		public void solve(PrintWriter out) {
            
		}


	}
	
	//Solution End
	
	
	//Template start	
	interface ISolution {
		void input(CodeJamReader in) throws IOException;
		void solve(PrintWriter out);
	}
	
	static class Solver implements Callable<String> {

		ISolution sol;
		
		Solver(ISolution sol) {
			this.sol = sol;
		}

		@Override
		public String call() throws Exception {
			StringWriter out = new StringWriter();
			sol.solve(new PrintWriter(out));
			return out.toString();
		}
		
	}

	public void run() {
		try {
			CodeJamReader in = new CodeJamReader(new BufferedReader(new FileReader(file + ".in")));
			out = new PrintWriter(file + ".out");
			
			ScheduledThreadPoolExecutor service = new ScheduledThreadPoolExecutor(7);
			
			int testcases = in.nextInt();
			Future<String>[] ts = new Future[testcases];
			
			for (int test = 0; test < testcases; ++test) {
				ISolution sol = new Solution();
				sol.input(in);
				ts[test] = service.submit(new Solver(sol));
			}
			
			for (int test = 0; test < testcases; ++test) {
				while (!ts[test].isDone()) {
					Thread.sleep(500);
				}
				System.out.println("Testcase " + test);
				out.print("Case #" + (test + 1) + ": ");
				out.print(ts[test].get());
			}
			service.shutdown();
			
			out.close();
			System.out.println("END");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	static class CodeJamReader {
		public CodeJamReader(BufferedReader in) {
			this.in = in;
			eat("");
		}
		
		private StringTokenizer st;
		private BufferedReader in;
		
		void eat(String s) {
			st = new StringTokenizer(s);
		}
		
		String next() throws IOException {
			while (!st.hasMoreTokens()) {
				String line = in.readLine();
				if (line == null) {
					return null;
				}
				eat(line);
			}
			return st.nextToken();
		}
		
		int nextInt() throws IOException {
			return Integer.parseInt(next());
		}
		
		long nextLong() throws IOException {
			return Long.parseLong(next());
		}
		
		double nextDouble() throws IOException {
			return Double.parseDouble(next());
		}
	}
	
	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		new Template().run();
	}
	
}

