package com.ntf.codejam2014rounda;

import java.io.*;
import java.util.Arrays;
import java.util.Locale;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class ChargingChaos implements Runnable {

	private PrintWriter out;
	final String file = "code-jam-2014-round-1a/A/A-large-practice";
	Random rnd = new Random(42);

	/**
	 * Solution here
	 * 
	 */
	static class Solution {

        int N, L;
        long[] a, b;
		
		Solution(CodeJamReader in) throws IOException {
            N = in.nextInt();
            L = in.nextInt();
            a = new long[N];
            b = new long[N];
            for (int i = 0; i < N; ++i) {
                String s = in.next();
                for (int j = 0; j < L; ++j) {
                    if (s.charAt(j) == '1') {
                        a[i] |= 1L << j;
                    }
                }
            }
            for (int i = 0; i < N; ++i) {
                String s = in.next();
                for (int j = 0; j < L; ++j) {
                    if (s.charAt(j) == '1') {
                        b[i] |= 1L << j;
                    }
                }
            }
		}
		
		void solve(PrintWriter out) {
            Arrays.sort(a);
            Arrays.sort(b);
            int ans = Arrays.equals(a, b) ? 0 : L + 1;
            for (int i = 0; i < N; ++i) {
                long[] a1 = new long[N];
                long mask = a[0] ^ b[i];
                for (int j = 0; j < N; ++j) {
                    a1[j] = a[j] ^ mask;
                }
                Arrays.sort(a1);
                if (Arrays.equals(a1, b)) {
                    ans = Math.min(ans, Long.bitCount(mask));
                }
            }
            out.println(ans == L + 1 ? "NOT POSSIBLE" : ans);
		}
	}
	
	//Solution End
	
	
	//Template start	
	static class Solver implements Callable<String> {

		Solution sol;
		
		Solver(Solution sol) {
			this.sol = sol;
		}

		@Override
		public String call() throws Exception {
			StringWriter out = new StringWriter();
			sol.solve(new PrintWriter(out));
			return out.toString();
		}
		
	}

	public void run() {
		try {
			CodeJamReader in = new CodeJamReader(new BufferedReader(new FileReader(file + ".in")));
			out = new PrintWriter(file + ".out");
			
			ScheduledThreadPoolExecutor service = new ScheduledThreadPoolExecutor(7);
			
			int testcases = in.nextInt();
			Future<String>[] ts = new Future[testcases];
			
			for (int test = 0; test < testcases; ++test) {
				ts[test] = service.submit(new Solver(new Solution(in)));
			}
			
			for (int test = 0; test < testcases; ++test) {
				while (!ts[test].isDone()) {
					Thread.sleep(500);
				}
				System.out.println("Testcase " + test);
				out.print("Case #" + (test + 1) + ": ");
				out.print(ts[test].get());
			}
			service.shutdown();
			
			out.close();
			System.out.println("END");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	static class CodeJamReader {
		public CodeJamReader(BufferedReader in) {
			this.in = in;
			eat("");
		}
		
		private StringTokenizer st;
		private BufferedReader in;
		
		void eat(String s) {
			st = new StringTokenizer(s);
		}
		
		String next() throws IOException {
			while (!st.hasMoreTokens()) {
				String line = in.readLine();
				if (line == null) {
					return null;
				}
				eat(line);
			}
			return st.nextToken();
		}
		
		int nextInt() throws IOException {
			return Integer.parseInt(next());
		}
		
		long nextLong() throws IOException {
			return Long.parseLong(next());
		}
		
		double nextDouble() throws IOException {
			return Double.parseDouble(next());
		}
	}
	
	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		new ChargingChaos().run();
	}
	
}

