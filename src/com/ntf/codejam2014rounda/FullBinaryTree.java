package com.ntf.codejam2014rounda;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class FullBinaryTree implements Runnable {

	private PrintWriter out;
	final String file = "code-jam-2014-round-1a/B/B-large-practice";
	Random rnd = new Random(42);
    


	/**
	 * Solution here
	 */
	static class Solution implements ISolution{
		
		public class Node {
	        int value;
	        Node left;
	        Node right;
	    }
	    
		Solution() {
			
		}
		
		protected int n;
        ArrayList<Integer>[] edges;
		@Override
		public void input(CodeJamReader in) throws IOException {
            
		    n = in.nextInt();
            edges = new ArrayList[n];
            
            for (int i = 0; i < n; ++i) {
                edges[i] = new ArrayList<Integer>();
            }
            
            for (int i = 0; i < n - 1; ++i) {
                int u = in.nextInt() - 1;
                int v = in.nextInt() - 1;
                edges[u].add(v);
                edges[v].add(u);
            }
		}
		
		public void solve(PrintWriter out) {
            int ans = n - 1; //cut everything out
            
            int[][] d = new int[n][n + 1];
            for (int[] ar : d) {
                Arrays.fill(ar, -1);
            }
            
            for (int i = 0; i < n; ++i) {
                ans = Math.min(ans, n - dfs(i, n, d));
            }
            out.println(ans);
		}
		
		/**
		 * 
		 * @param i root
		 * @param p parent
		 * @param d 
		 * @return
		 */
		private int dfs(int i, int p, int[][] d) {
            if (d[i][p] == -1) {
                int max1 = -1, max2 = -1;
                for (int j : edges[i]) {
                    if (j == p) {
                        continue;
                    }
                    int v = dfs(j, i, d);
                    if (v > max1) {
                        max2 = max1;
                        max1 = v;
                    } else if (v > max2) {
                        max2 = v;
                    }
                }
                if (max2 == -1) {
                    d[i][p] = 1;
                } else {
                    d[i][p] = 1 + max1 + max2;
                }
            }
            return d[i][p];
        }

	}
	
	//Solution End
	
	
	//Template start	
	interface ISolution {
		void input(CodeJamReader in) throws IOException;
		void solve(PrintWriter out);
	}
	
	static class Solver implements Callable<String> {

		ISolution sol;
		
		Solver(ISolution sol) {
			this.sol = sol;
		}

		@Override
		public String call() throws Exception {
			StringWriter out = new StringWriter();
			sol.solve(new PrintWriter(out));
			return out.toString();
		}
		
	}

	public void run() {
		try {
			CodeJamReader in = new CodeJamReader(new BufferedReader(new FileReader(file + ".in")));
			out = new PrintWriter(file + ".out");
			
			ScheduledThreadPoolExecutor service = new ScheduledThreadPoolExecutor(7);
			
			int testcases = in.nextInt();
			Future<String>[] ts = new Future[testcases];
			
			for (int test = 0; test < testcases; ++test) {
				ISolution sol = new Solution();
				sol.input(in);
				ts[test] = service.submit(new Solver(sol));
			}
			
			for (int test = 0; test < testcases; ++test) {
				while (!ts[test].isDone()) {
					Thread.sleep(500);
				}
				System.out.println("Testcase " + test);
				out.print("Case #" + (test + 1) + ": ");
				out.print(ts[test].get());
			}
			service.shutdown();
			
			out.close();
			System.out.println("END");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	static class CodeJamReader {
		public CodeJamReader(BufferedReader in) {
			this.in = in;
			eat("");
		}
		
		private StringTokenizer st;
		private BufferedReader in;
		
		void eat(String s) {
			st = new StringTokenizer(s);
		}
		
		String next() throws IOException {
			while (!st.hasMoreTokens()) {
				String line = in.readLine();
				if (line == null) {
					return null;
				}
				eat(line);
			}
			return st.nextToken();
		}
		
		int nextInt() throws IOException {
			return Integer.parseInt(next());
		}
		
		long nextLong() throws IOException {
			return Long.parseLong(next());
		}
		
		double nextDouble() throws IOException {
			return Double.parseDouble(next());
		}
	}
	
	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		new FullBinaryTree().run();
	}
	
}

